package datos;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import estructura.Usuario;

public class Json {

	String archivoJson;

	public Json(String ruta) {
		archivoJson = ruta;
	}

	public void setRuta(String archivo) {
		archivoJson = archivo;
	}

	public String nombreDeArchivo() {
		return archivoJson;
	}

	@SuppressWarnings("unchecked")
	public void guardarUsuarios(ArrayList<Usuario> usuarios) {
		JSONArray listaJsonUser = new JSONArray();
		for (int i = 0; i < usuarios.size(); i++) {
			JSONObject objetoUser = new JSONObject();
			objetoUser.put("nombre:", usuarios.get(i).getNombre());
			JSONArray listaJsonOf = new JSONArray();
			listaJsonOf.add(objetoUser);
			listaJsonUser.add(listaJsonOf);
		}
		try {
			FileWriter file = new FileWriter(archivoJson);
			file.write(listaJsonUser.toJSONString());
			file.flush();
			file.close();
		} catch (IOException e) {
		}
	}

	public ArrayList<Usuario> cargarUsuarios() {
		JSONParser parser = new JSONParser();
		ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
		try {
			Object obj = parser.parse(new FileReader(archivoJson));
			JSONArray listaJson = (JSONArray) obj;
			for (int i = 0; i < listaJson.size(); i++) {
				JSONArray listaJson1 = (JSONArray) listaJson.get(i);
				JSONObject Usuario = (JSONObject) listaJson1.get(0);
				String nombre = (String) Usuario.get("nombre:");
				Usuario user = new Usuario(nombre);
				usuarios.add(user);
			}
		} catch (FileNotFoundException e) {
			// manejo de error
		} catch (IOException e) {
			// manejo de error
		} catch (ParseException e) {
			// manejo de error
		}
		return usuarios;
	}
}