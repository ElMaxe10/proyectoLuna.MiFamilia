package estructura;

public class Historial {
	private String _directorio= "Recursos/Usuarios/";
	
	Publicacion nuevaPublicacion(Publicacion publicacion){
		return null;		
	}
	
	Publicacion guardarPublicacion(String directorio){
		return null;
	}
		
	Publicacion getPublicacion(String directorio){
		return null;
	}

	// historial es un directorio de directorios
	// quiere decir que por cada usuario le corresponde una carpeta, la cual
	// contiene todos las publicaciones
	// la direccion seria Recursos/Usuarios/*miUsuario*
	// y dentro de la carpeta *miUsuario*, toda las subcarpetas con las publicaciones
	// Se veria de la siguiente forma:
	
	/* carpeta: miUsuario* subcarpeta: 17/1/2017 subcarpeta: 16/1/2017
	 * subcarpeta: 15/1/2017
	 * 
	 * cada subcarpeta se llama con la fecha en que fue hecha la publicacion/ es
	 * una carpeta por publicacion a que solo se puede publicar una cosa al dia
	 * 
	 * o sea que cada vez que se publica algo nuevo, automaticamente se crea una
	 * nueva carpeta(con nombre de la fecha del dia) y se la guarda
	 * 
	 * publicacion() tambien es un objeto
	 */

	/* ATRIBUTOS: (todos Strings) direccion del directorio = inicializado seria Recursos/Usuarios/*miUsuario*
	 * 
	 * METODOS: getters y setters
	 * 
	 * (1) -> metodo nueva__publicaciones recibe una publicacion,crea una
	 * carpeta nueva, y la guarda dentro de la carpeta) como la guarda?? con un
	 * metodo publicaciones.guardar(directorio) que seria un metodo de
	 * publicacion() que recibe un directorio y se guarda dentro de �l
	 */

}
