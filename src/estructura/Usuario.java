package estructura;

public class Usuario {
	private String _nombre, _apellido, _sexo, _direccion, _padres, _hermanos, _mascotas, _colegio, _maestro;
	private int _edad, _telefono;
	Historial historial= null;
	
	public Usuario(String nombre, String apellido, String sexo, String direccion, String padres, String hermanos, String mascotas, String colegio, String maestro, int edad, int telefono) {
		_nombre=	nombre;
		_apellido=	apellido;
		_edad=		edad;
		_sexo= 		sexo;
		_direccion= direccion;
		_telefono=	telefono;
		_padres=	padres;
		_hermanos=	hermanos;
		_mascotas=	mascotas;
		_colegio=	colegio;
		_maestro=	maestro;
	}

	public Usuario(String nombre) {
	}
	
	public Historial crearHistorial(){
		return historial;		
	}

	public String getNombre() {
		return _nombre;
	}
	
	public String getUsuario(){
		return _nombre + _apellido;
	}
	
	public String setNombre() {
		return _nombre;
	}
	
	public String setApellido() {
		return _apellido;
	}
	
	public int setEdad() {
		return _edad;
	}
	
	public String setSexo() {
		return _sexo;
	}
	
	public String setDireccion() {
		return _direccion;
	}
	
	public int setTelefono() {
		return _telefono;
	}
	
	public String setPadres() {
		return _padres;
	}
	
	public String setHermanos() {
		return _hermanos;
	}
	
	public String setMascotas() {
		return _mascotas;
	}
	
	public String setColegio() {
		return _colegio;
	}
	
	public String setMaestro() {
		return _maestro;
	}
	
	public String toString(){
		return _nombre + _apellido + _edad + _sexo + _direccion + _telefono + _padres + _hermanos + _mascotas + _colegio + _maestro;
	}

	/*
	 * ATRIBUTOS: (todos Strings) nombre apellido edad sexo donde_vive direccion
	 * telefono papas hermanos mascotas colegio maestro direccion de imagen
	 * historial = null (1)
	 * 
	 * METODOS: getters y setters
	 * 
	 * (1) -> metodo crear_historial (crea un nuevo historial(Objeto) )
	 */

}
