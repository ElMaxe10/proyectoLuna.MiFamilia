package estructura;

import java.util.ArrayList;
import java.util.Iterator;

public class Publicaciones {
	private static ArrayList<Publicacion> _publicaciones;

	public Publicaciones() {
		_publicaciones = new ArrayList<Publicacion>();
	}
	
	public void guardar(Publicacion p) {
		_publicaciones.add(p);
	}

	public static void agregar(Publicacion p) {
		_publicaciones.add(p);
	}

	public void remover(Publicacion o) {
		_publicaciones.remove(o);
	}

	public Publicacion get(int index) {
		return _publicaciones.get(index);
	}

	public int size() {
		return _publicaciones.size();
	}

	public boolean contains(Publicacion o) {
		for (Publicacion of : _publicaciones)
			if (of.equals(o))
				return true;
		return false;
	}

	public Iterator<Publicacion> iterator() {
		return _publicaciones.iterator();
	}

	public ArrayList<Publicacion> getOfertas() {
		return _publicaciones;
	}

	public String toString() {
		String ret = "";
		for (Publicacion o : _publicaciones) {
			ret += o;
			ret += "\n";
		}
		return ret;
	}

}
