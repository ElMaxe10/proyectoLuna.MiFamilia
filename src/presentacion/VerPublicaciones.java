package presentacion;

import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class VerPublicaciones extends JDialog {
	private final JPanel frame = new JPanel();

	public static void main(String[] args) {
		try {
			Home dialog = new Home();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public VerPublicaciones() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		frame.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(frame, BorderLayout.CENTER);
		frame.setLayout(null);
	}
}
