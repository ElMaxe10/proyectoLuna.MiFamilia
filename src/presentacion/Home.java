package presentacion;

import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Home extends JDialog {
	private final JPanel frame = new JPanel();

	public static void main(String[] args) {
		try {
			Home dialog = new Home();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Home() {
		setTitle("Home");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		frame.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(frame, BorderLayout.CENTER);
		frame.setLayout(null);
		
		JButton btnVerPublicaicones = new JButton("Ver Publicaciones");
		btnVerPublicaicones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				verPublicaiones();
			}

			private void verPublicaiones() {
				new VerPublicaciones().setVisible(true);
			}
		});
		btnVerPublicaicones.setBounds(39, 44, 196, 25);
		frame.add(btnVerPublicaicones);
		
		JButton btnNewButton = new JButton("Agregar Publicacion");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				agregarPublicacion();
			}

			private void agregarPublicacion() {
				new AgregarPublicacion().setVisible(true);
			}
		});
		btnNewButton.setBounds(42, 97, 193, 25);
		frame.add(btnNewButton);
	}
}
