package presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class LogIn {

	private JFrame frame = new JFrame(); 
	private JTextField lblUsuario;
	private JPasswordField lblContraseña;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LogIn window = new LogIn();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public LogIn() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		lblUsuario = new JTextField();
		lblUsuario.setBounds(239, 90, 125, 19);
		frame.getContentPane().add(lblUsuario);
		lblUsuario.setColumns(10);

		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(120, 92, 70, 15);
		frame.getContentPane().add(lblNombre);

		JLabel lblContrasea = new JLabel("Contraseña:");
		lblContrasea.setBounds(120, 140, 114, 15);
		frame.getContentPane().add(lblContrasea);

		lblContraseña = new JPasswordField();
		lblContraseña.setBounds(239, 138, 125, 19);
		frame.getContentPane().add(lblContraseña);

		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String usuario = "admin";
				String contraseña = "123";

				String Pass = new String(lblContraseña.getPassword());

				if (lblUsuario.getText().equals(usuario) && Pass.equals(contraseña))
					abrirHome();
				else
					JOptionPane.showMessageDialog(null, "Nombre y/o Contraseña incorrecta");
			}

			private void abrirHome() {
				new Home().setVisible(true);
				frame.hide();
			}
		});
		btnEntrar.setBounds(184, 188, 117, 25);
		frame.getContentPane().add(btnEntrar);
	}
	
	
}
