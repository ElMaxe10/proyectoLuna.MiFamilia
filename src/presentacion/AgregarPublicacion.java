package presentacion;


import java.awt.BorderLayout;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import estructura.Publicacion;
import estructura.Publicaciones;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AgregarPublicacion extends JDialog {
	private final JPanel frame = new JPanel();
	private JTextField textField;

	public static void main(String[] args) {
		try {
			Home dialog = new Home();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public AgregarPublicacion() {
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		frame.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(frame, BorderLayout.CENTER);
		frame.setLayout(null);
		
		JLabel lblQueQuieresPublicar = new JLabel("¿Qué quieres publicar?");
		lblQueQuieresPublicar.setBounds(29, 16, 209, 24);
		frame.add(lblQueQuieresPublicar);
		
		textField = new JTextField();
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		textField.setBounds(22, 52, 399, 173);
		frame.add(textField);
		textField.setColumns(10);
		
		JButton btnAgregarPublicacion = new JButton("Agregar publicacion");
		btnAgregarPublicacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				agregarPublicacion();
				System.out.println(textField.getText());
			}

			private void agregarPublicacion() {
				Publicacion p= new Publicacion(null, textField.getText(), null);
//				Publicaciones.agregar(p);
			}
		});
		btnAgregarPublicacion.setBounds(235, 237, 190, 25);
		frame.add(btnAgregarPublicacion);
	}
}
