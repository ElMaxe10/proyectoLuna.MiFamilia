package grafico;

import java.applet.Applet;
import java.applet.AudioClip;

public class Sound {
	public static final AudioClip bienvenido = Applet.newAudioClip(Sound.class.getResource("bienvenido.wav"));
	public static final AudioClip botonOferentes = Applet.newAudioClip(Sound.class.getResource("botonOferentes.wav"));
	public static final AudioClip botonOferentesGanancias = Applet
			.newAudioClip(Sound.class.getResource("botonOferentesGanancias.wav"));
	public static final AudioClip botonOferentesSeleccionados = Applet
			.newAudioClip(Sound.class.getResource("botonOferentesSeleccionados.wav"));
	public static final AudioClip comenzar = Applet.newAudioClip(Sound.class.getResource("BotonComenzar.wav"));
	public static final AudioClip ventanaPrincipal = Applet
			.newAudioClip(Sound.class.getResource("VentanaPrinpal1.wav"));
	public static final AudioClip botonSaltarDia = Applet.newAudioClip(Sound.class.getResource("botonSaltarDia.wav"));
	public static final AudioClip VentanaResultados = Applet
			.newAudioClip(Sound.class.getResource("VentanaResultados.wav"));
	public static final AudioClip listaOferentesSeleccionados = Applet
			.newAudioClip(Sound.class.getResource("listaOferentesSeleccionados.wav"));
	public static final AudioClip listaOferentes = Applet.newAudioClip(Sound.class.getResource("listaOferentes.wav"));
	public static final AudioClip ganancias = Applet.newAudioClip(Sound.class.getResource("ganancias.wav"));
	public static final AudioClip boton = Applet.newAudioClip(Sound.class.getResource("beep-08b.wav"));
	public static final AudioClip enter = Applet.newAudioClip(Sound.class.getResource("enter.wav"));
	public static final AudioClip q = Applet.newAudioClip(Sound.class.getResource("q.wav"));
}