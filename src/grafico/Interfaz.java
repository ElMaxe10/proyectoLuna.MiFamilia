package grafico;

import javax.swing.JFrame;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;

//import Juego.Sound;
import datos.Json;
//import estructura.Adjudicador;
//import estructura.Elemento;
//import estructura.Oferta;
import estructura.Usuario;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Vector;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.Random;

public class Interfaz extends javax.swing.JFrame {

	JFrame ventanaLogin; // escenas
	JFrame ventanaRegistro;
	JFrame ventanaSeleccionUsuario;
	JFrame ventanaMuro;
	JFrame ventanaPerfil;
	JFrame ventanaSobreMi;

	JFrame ventanaDia;

	int contadorRegistro = 0;

	boolean existeUsuarioPrincipal = false; // en este caso no existe

	// ESTO DEBE SER UN OBJETO "PERSONA"
	String sexoFER = "nene"; // OBJETO YA CREADO
	String nombre1FER = "fer";
	String apellidoFER = "torres";
	String donde_viveFER = "buenos aires";
	String direccionFER = "casta�eda 1234";
	String telefonoFER = "2312 321321 3132";
	String papasFER = "eva y pedro";
	String hermanosFER = "pedro";
	String mascotasFER = "pepito y pepote";
	String colegioFER = "escuela 4";
	String maestroFER = "alicia";
	// ...........................

	String sexo = ""; // OBJETO VACIO PARA CREAR UN USUARIO NUEVO
	String nombre1 = "";
	String apellido = "";
	String donde_vive = "";
	String direccion = "";
	String telefono = "";
	String papas = "";
	String hermanos = "";
	String mascotas = "";
	String colegio = "";
	String maestro = "";
	//////////////////////

	// TODO
	// Json workJson = new Json("Configuraciones.json");
	// Configuraciones _config = workJson.cargarDatos();

	public Interfaz() {
		initialize();
	}

	private void initialize() {

		//////////////////////// ventanas
		//////////////////////// /////////////////////////////////////////
		ventanaLogin = new JFrame();
		ventanaLogin.setUndecorated(true);
		ventanaLogin.getContentPane().setBackground(Color.decode("#800000"));
		ventanaLogin.setBounds(100, 100, 800, 600);
		ventanaLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventanaLogin.setTitle("Mi Familia");
		ventanaLogin.getContentPane().setLayout(null);
		ventanaLogin.setVisible(false);

		ventanaRegistro = new JFrame();
		ventanaRegistro.setUndecorated(true);
		ventanaRegistro.getContentPane().setBackground(Color.decode("#800000"));
		ventanaRegistro.setBounds(100, 100, 800, 600);
		ventanaRegistro.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventanaRegistro.setTitle("Mi Familia");
		ventanaRegistro.getContentPane().setLayout(null);
		ventanaRegistro.setVisible(false);

		ventanaSeleccionUsuario = new JFrame();
		ventanaSeleccionUsuario.setUndecorated(true);
		ventanaSeleccionUsuario.getContentPane().setBackground(Color.decode("#800000"));
		ventanaSeleccionUsuario.setBounds(100, 100, 800, 600);
		ventanaSeleccionUsuario.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventanaSeleccionUsuario.setTitle("Mi Familia");
		ventanaSeleccionUsuario.getContentPane().setLayout(null);
		ventanaSeleccionUsuario.setVisible(false);

		ventanaMuro = new JFrame();
		ventanaMuro.setUndecorated(true);
		ventanaMuro.getContentPane().setBackground(Color.decode("#800000"));
		ventanaMuro.setBounds(100, 100, 800, 600);
		ventanaMuro.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventanaMuro.setTitle("Mi Familia");
		ventanaMuro.getContentPane().setLayout(null);
		ventanaMuro.setVisible(false);

		ventanaPerfil = new JFrame();
		ventanaPerfil.setUndecorated(true);
		ventanaPerfil.getContentPane().setBackground(Color.decode("#800000"));
		ventanaPerfil.setBounds(100, 100, 800, 600);
		ventanaPerfil.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventanaPerfil.setTitle("Mi Familia");
		ventanaPerfil.getContentPane().setLayout(null);
		ventanaPerfil.setVisible(false);

		ventanaSobreMi = new JFrame();
		ventanaSobreMi.setUndecorated(true);
		ventanaSobreMi.getContentPane().setBackground(Color.decode("#800000"));
		ventanaSobreMi.setBounds(100, 100, 800, 600);
		ventanaSobreMi.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventanaSobreMi.setTitle("Mi Familia");
		ventanaSobreMi.getContentPane().setLayout(null);
		ventanaSobreMi.setVisible(false);

		ventanaDia = new JFrame();
		ventanaDia.setUndecorated(true);
		ventanaDia.getContentPane().setBackground(Color.decode("#800000"));
		ventanaDia.setBounds(0, 0, 900, 548);
		ventanaDia.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventanaDia.setTitle("Mi Familia");
		ventanaDia.getContentPane().setLayout(null);
		ventanaDia.setVisible(false);

		/////////////////////////////////////////////////////////////////

		/////////////////////////// elementos de escenas
		/////////////////////////// //////////////////////////////////////

		ImageIcon iconoCerrar = new ImageIcon("Recursos/Objetos/Botones/Cerrar/Imagenes/cerrar.png");
		ImageIcon iconoSeguir = new ImageIcon("Recursos/Objetos/Botones/Adelante/Imagenes/seguir.png");

		// elementos login
		// _________________________________________________________________________
		JButton botonEntrar = new JButton("");
		ImageIcon iconoEntrar = new ImageIcon("Recursos/Ventanas/Login/entrar.png");
		botonEntrar.setIcon(iconoEntrar);
		botonEntrar.setBounds(243, 407, 296, 116);
		ventanaLogin.getContentPane().add(botonEntrar);

		JButton cerrarLogin = new JButton("");
		cerrarLogin.setBounds(700, 0, 82, 70);
		cerrarLogin.setIcon(iconoCerrar);
		ventanaLogin.getContentPane().add(cerrarLogin);

		JLabel fondoLogin = new JLabel();
		fondoLogin.setIcon(new ImageIcon("Recursos/Ventanas/Login/fondo.png"));
		fondoLogin.setBounds(0, 0, 800, 600);
		ventanaLogin.getContentPane().add(fondoLogin);

		// elementos seleccionUsuario
		// ____________________________________________________________________________
		JButton botonOtros = new JButton("");
		ImageIcon iconoOtros = new ImageIcon("Recursos/Ventanas/Login/otros.png");
		botonOtros.setIcon(iconoOtros);
		botonOtros.setBounds(608, 519, 186, 73);
		ventanaSeleccionUsuario.getContentPane().add(botonOtros);

		JButton botonNuevoUsuario = new JButton("");
		ImageIcon iconoNuevoUsuario = new ImageIcon("Recursos/Ventanas/Login/NuevoUsuario.png");
		botonNuevoUsuario.setIcon(iconoNuevoUsuario);
		botonNuevoUsuario.setBounds(15, 470, 298, 118);
		ventanaSeleccionUsuario.getContentPane().add(botonNuevoUsuario);

		JButton botonUsuarioPrincipal = new JButton("");
		ImageIcon iconoUsuarioPrincipal = new ImageIcon("Recursos/Ventanas/Login/principalEjemplo.png");
		botonUsuarioPrincipal.setIcon(iconoUsuarioPrincipal);
		botonUsuarioPrincipal.setBounds(285, 182, 240, 240);
		ventanaSeleccionUsuario.getContentPane().add(botonUsuarioPrincipal);

		JButton cerrarSeleccionUsuario = new JButton("555555");
		cerrarSeleccionUsuario.setBounds(700, 0, 82, 70);
		cerrarSeleccionUsuario.setIcon(iconoCerrar);
		ventanaSeleccionUsuario.getContentPane().add(cerrarSeleccionUsuario);

		JLabel fondoSeleccionUsuario = new JLabel();
		fondoSeleccionUsuario.setIcon(new ImageIcon("Recursos/Ventanas/Login/fondo1.png"));
		fondoSeleccionUsuario.setBounds(0, 0, 800, 600);
		ventanaSeleccionUsuario.getContentPane().add(fondoSeleccionUsuario);

		// elementos registro
		// _________________________________________________________________________________
		JButton cerrarRegistro = new JButton("");
		cerrarRegistro.setBounds(700, 0, 82, 70);
		cerrarRegistro.setIcon(iconoCerrar);
		ventanaRegistro.getContentPane().add(cerrarRegistro);

		JButton seguirRegistro = new JButton("");
		seguirRegistro.setBounds(660, 430, 101, 123);
		seguirRegistro.setIcon(iconoSeguir);
		ventanaRegistro.getContentPane().add(seguirRegistro);

		// --------
		String textoRegistro0 = "�SOS NENE O NENA?";
		JLabel preguntaRegistro0 = new JLabel(textoRegistro0);
		preguntaRegistro0.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 40));
		preguntaRegistro0.setBounds(29, 149, 600, 100);
		preguntaRegistro0.setForeground(Color.decode("#000000"));
		ventanaRegistro.getContentPane().add(preguntaRegistro0);
		preguntaRegistro0.setVisible(false);

		JLabel pictonene = new JLabel();
		pictonene.setIcon(new ImageIcon("Recursos/Ventanas/Formulario/nene.png"));
		pictonene.setBounds(100, 260, 200, 200);
		ventanaRegistro.getContentPane().add(pictonene);
		pictonene.setVisible(false);

		JLabel pictonena = new JLabel();
		pictonena.setIcon(new ImageIcon("Recursos/Ventanas/Formulario/nena.png"));
		pictonena.setBounds(300, 260, 200, 200);
		ventanaRegistro.getContentPane().add(pictonena);
		pictonena.setVisible(false);

		JRadioButton neneOK = new JRadioButton();
		neneOK.setBounds(200, 520, 20, 20);
		ventanaRegistro.getContentPane().add(neneOK);
		neneOK.setVisible(false);

		JRadioButton nenaOK = new JRadioButton();
		nenaOK.setBounds(400, 520, 20, 20);
		ventanaRegistro.getContentPane().add(nenaOK);
		nenaOK.setVisible(false);

		// --------

		String textoRegistro1 = "�C�MO TE LLAMAS?";
		JLabel preguntaRegistro1 = new JLabel(textoRegistro1);
		preguntaRegistro1.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 40));
		preguntaRegistro1.setBounds(29, 149, 600, 100);
		preguntaRegistro1.setForeground(Color.decode("#8B0000"));
		ventanaRegistro.getContentPane().add(preguntaRegistro1);
		preguntaRegistro1.setVisible(false);

		String textoRegistro2 = "�CU�L ES TU APELLIDO?";
		JLabel preguntaRegistro2 = new JLabel(textoRegistro2);
		preguntaRegistro2.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 40));
		preguntaRegistro2.setBounds(29, 300, 600, 100);
		preguntaRegistro2.setForeground(Color.decode("#8B0000"));
		ventanaRegistro.getContentPane().add(preguntaRegistro2);
		preguntaRegistro2.setVisible(false);

		JTextField texto_ingreso_registro_1 = new JTextField("");
		texto_ingreso_registro_1.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 40));
		texto_ingreso_registro_1.setBounds(129, 249, 400, 70);
		ventanaRegistro.getContentPane().add(texto_ingreso_registro_1);
		texto_ingreso_registro_1.setVisible(false);

		JTextField texto_ingreso_registro_2 = new JTextField("");
		texto_ingreso_registro_2.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 40));
		texto_ingreso_registro_2.setBounds(129, 400, 400, 70);
		ventanaRegistro.getContentPane().add(texto_ingreso_registro_2);
		texto_ingreso_registro_2.setVisible(false);

		JLabel pictotu_nombre = new JLabel();
		pictotu_nombre.setIcon(new ImageIcon("Recursos/Ventanas/Formulario/tu_nombre.png"));
		pictotu_nombre.setBounds(550, 149, 200, 200);
		ventanaRegistro.getContentPane().add(pictotu_nombre);
		pictotu_nombre.setVisible(false);

		// --------

		String textoRegistro3 = "�DONDE VIVIS?";
		JLabel preguntaRegistro3 = new JLabel(textoRegistro3);
		preguntaRegistro3.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 40));
		preguntaRegistro3.setBounds(29, 149, 600, 100);
		preguntaRegistro3.setForeground(Color.decode("#8B0000"));
		ventanaRegistro.getContentPane().add(preguntaRegistro3);
		preguntaRegistro3.setVisible(false);

		JLabel pictodonde_vivis = new JLabel();
		pictodonde_vivis.setIcon(new ImageIcon("Recursos/Ventanas/Formulario/donde_vivis.png"));
		pictodonde_vivis.setBounds(280, 340, 200, 200);
		ventanaRegistro.getContentPane().add(pictodonde_vivis);
		pictodonde_vivis.setVisible(false);

		JTextField texto_ingreso_registro_3 = new JTextField("");
		texto_ingreso_registro_3.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 40));
		texto_ingreso_registro_3.setBounds(129, 249, 400, 70);
		ventanaRegistro.getContentPane().add(texto_ingreso_registro_3);
		texto_ingreso_registro_3.setVisible(false);

		// --------

		String textoRegistro4 = "�CUAL ES TU DIRECCI�N?";
		JLabel preguntaRegistro4 = new JLabel(textoRegistro4);
		preguntaRegistro4.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 40));
		preguntaRegistro4.setBounds(29, 149, 600, 100);
		preguntaRegistro4.setForeground(Color.decode("#8B0000"));
		ventanaRegistro.getContentPane().add(preguntaRegistro4);
		preguntaRegistro4.setVisible(false);

		JLabel pictodireccion = new JLabel();
		pictodireccion.setIcon(new ImageIcon("Recursos/Ventanas/Formulario/direccion.png"));
		pictodireccion.setBounds(280, 340, 200, 200);
		ventanaRegistro.getContentPane().add(pictodireccion);
		pictodireccion.setVisible(false);

		JTextField texto_ingreso_registro_4 = new JTextField("");
		texto_ingreso_registro_4.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 40));
		texto_ingreso_registro_4.setBounds(129, 249, 400, 70);
		ventanaRegistro.getContentPane().add(texto_ingreso_registro_4);
		texto_ingreso_registro_4.setVisible(false);

		// --------

		String textoRegistro5 = "�CUAL ES EL TELEFONO DE CASA?";
		JLabel preguntaRegistro5 = new JLabel(textoRegistro5);
		preguntaRegistro5.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 40));
		preguntaRegistro5.setBounds(29, 149, 900, 100);
		preguntaRegistro5.setForeground(Color.decode("#8B0000"));
		ventanaRegistro.getContentPane().add(preguntaRegistro5);
		preguntaRegistro5.setVisible(false);

		JLabel pictotelefono = new JLabel();
		pictotelefono.setIcon(new ImageIcon("Recursos/Ventanas/Formulario/telefono.png"));
		pictotelefono.setBounds(280, 340, 200, 200);
		ventanaRegistro.getContentPane().add(pictotelefono);
		pictotelefono.setVisible(false);

		JTextField texto_ingreso_registro_5 = new JTextField("");
		texto_ingreso_registro_5.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 40));
		texto_ingreso_registro_5.setBounds(129, 249, 400, 70);
		ventanaRegistro.getContentPane().add(texto_ingreso_registro_5);
		texto_ingreso_registro_5.setVisible(false);

		// --------

		String textoRegistro6 = "�COMO SE LLAMAN TUS PAPAS?";
		JLabel preguntaRegistro6 = new JLabel(textoRegistro6);
		preguntaRegistro6.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 40));
		preguntaRegistro6.setBounds(29, 149, 900, 100);
		preguntaRegistro6.setForeground(Color.decode("#8B0000"));
		ventanaRegistro.getContentPane().add(preguntaRegistro6);
		preguntaRegistro6.setVisible(false);

		JLabel pictopapa = new JLabel();
		pictopapa.setIcon(new ImageIcon("Recursos/Ventanas/Formulario/papa.png"));
		pictopapa.setBounds(380, 340, 200, 200);
		ventanaRegistro.getContentPane().add(pictopapa);
		pictopapa.setVisible(false);

		JLabel pictomama = new JLabel();
		pictomama.setIcon(new ImageIcon("Recursos/Ventanas/Formulario/mama.png"));
		pictomama.setBounds(180, 340, 200, 200);
		ventanaRegistro.getContentPane().add(pictomama);
		pictomama.setVisible(false);

		JTextField texto_ingreso_registro_6 = new JTextField("MAMA.. Y PAPA..");
		texto_ingreso_registro_6.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 40));
		texto_ingreso_registro_6.setBounds(129, 249, 600, 70);
		ventanaRegistro.getContentPane().add(texto_ingreso_registro_6);
		texto_ingreso_registro_6.setVisible(false);

		// --------

		String textoRegistro7 = "�COMO SE LLAMAN TUS HERMANOS?";
		JLabel preguntaRegistro7 = new JLabel(textoRegistro7);
		preguntaRegistro7.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 40));
		preguntaRegistro7.setBounds(29, 149, 900, 100);
		preguntaRegistro7.setForeground(Color.decode("#8B0000"));
		ventanaRegistro.getContentPane().add(preguntaRegistro7);
		preguntaRegistro7.setVisible(false);

		JLabel pictohermano = new JLabel();
		pictohermano.setIcon(new ImageIcon("Recursos/Ventanas/Formulario/hermano.png"));
		pictohermano.setBounds(280, 340, 200, 200);
		ventanaRegistro.getContentPane().add(pictohermano);
		pictohermano.setVisible(false);

		JTextField texto_ingreso_registro_7 = new JTextField("");
		texto_ingreso_registro_7.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
		texto_ingreso_registro_7.setBounds(129, 249, 600, 70);
		ventanaRegistro.getContentPane().add(texto_ingreso_registro_7);
		texto_ingreso_registro_7.setVisible(false);

		// --------

		String textoRegistro8 = "�COMO SE LLAMAN TUS MASCOTAS?";
		JLabel preguntaRegistro8 = new JLabel(textoRegistro8);
		preguntaRegistro8.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 40));
		preguntaRegistro8.setBounds(29, 149, 900, 100);
		preguntaRegistro8.setForeground(Color.decode("#8B0000"));
		ventanaRegistro.getContentPane().add(preguntaRegistro8);
		preguntaRegistro8.setVisible(false);

		JLabel pictoperro = new JLabel();
		pictoperro.setIcon(new ImageIcon("Recursos/Ventanas/Formulario/perro.png"));
		pictoperro.setBounds(380, 340, 200, 200);
		ventanaRegistro.getContentPane().add(pictoperro);
		pictoperro.setVisible(false);

		JLabel pictogato = new JLabel();
		pictogato.setIcon(new ImageIcon("Recursos/Ventanas/Formulario/gato.png"));
		pictogato.setBounds(180, 340, 200, 200);
		ventanaRegistro.getContentPane().add(pictogato);
		pictogato.setVisible(false);

		JTextField texto_ingreso_registro_8 = new JTextField("");
		texto_ingreso_registro_8.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 40));
		texto_ingreso_registro_8.setBounds(129, 249, 600, 70);
		ventanaRegistro.getContentPane().add(texto_ingreso_registro_8);
		texto_ingreso_registro_8.setVisible(false);

		// --------

		String textoRegistro9 = "�COMO SE LLAMA TU COLEGIO?";
		JLabel preguntaRegistro9 = new JLabel(textoRegistro9);
		preguntaRegistro9.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 40));
		preguntaRegistro9.setBounds(29, 149, 900, 100);
		preguntaRegistro9.setForeground(Color.decode("#8B0000"));
		ventanaRegistro.getContentPane().add(preguntaRegistro9);
		preguntaRegistro9.setVisible(false);

		JLabel pictocolegio = new JLabel();
		pictocolegio.setIcon(new ImageIcon("Recursos/Ventanas/Formulario/colegio.png"));
		pictocolegio.setBounds(280, 340, 200, 200);
		ventanaRegistro.getContentPane().add(pictocolegio);
		pictocolegio.setVisible(false);

		JTextField texto_ingreso_registro_9 = new JTextField("");
		texto_ingreso_registro_9.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 40));
		texto_ingreso_registro_9.setBounds(129, 249, 600, 70);
		;
		ventanaRegistro.getContentPane().add(texto_ingreso_registro_9);
		texto_ingreso_registro_9.setVisible(false);

		// --------

		String textoRegistro10 = "�COMO SE LLAMA TU MAESTRO?";
		JLabel preguntaRegistro10 = new JLabel(textoRegistro10);
		preguntaRegistro10.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 40));
		preguntaRegistro10.setBounds(29, 149, 900, 100);
		preguntaRegistro10.setForeground(Color.decode("#8B0000"));
		ventanaRegistro.getContentPane().add(preguntaRegistro10);
		preguntaRegistro10.setVisible(false);

		JLabel pictomaestra = new JLabel();
		pictomaestra.setIcon(new ImageIcon("Recursos/Ventanas/Formulario/maestra.png"));
		pictomaestra.setBounds(280, 340, 200, 200);
		ventanaRegistro.getContentPane().add(pictomaestra);
		pictomaestra.setVisible(false);

		JTextField texto_ingreso_registro_10 = new JTextField("");
		texto_ingreso_registro_10.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 40));
		texto_ingreso_registro_10.setBounds(129, 249, 600, 70);
		ventanaRegistro.getContentPane().add(texto_ingreso_registro_10);
		texto_ingreso_registro_10.setVisible(false);

		// --------

		String textoRegistro11 = "SUBI TU FOTO";
		JLabel preguntaRegistro11 = new JLabel(textoRegistro11);
		preguntaRegistro11.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 40));
		preguntaRegistro11.setBounds(29, 149, 900, 100);
		preguntaRegistro11.setForeground(Color.decode("#8B0000"));
		ventanaRegistro.getContentPane().add(preguntaRegistro11);
		preguntaRegistro11.setVisible(false);

		JButton botonsubirRegistro = new JButton("");
		ImageIcon iconosubirRegistro = new ImageIcon("Recursos/Ventanas/Formulario/subir.png");
		botonsubirRegistro.setIcon(iconosubirRegistro);
		botonsubirRegistro.setBounds(306, 249, 186, 73);
		ventanaRegistro.getContentPane().add(botonsubirRegistro);
		botonsubirRegistro.setVisible(false);

		JLabel pictofoto = new JLabel();
		pictofoto.setIcon(new ImageIcon("Recursos/Ventanas/Formulario/foto.png"));
		pictofoto.setBounds(280, 340, 200, 200);
		ventanaRegistro.getContentPane().add(pictofoto);
		pictofoto.setVisible(false);

		// --------

		String textoRegistro12 = "DEBES SELECCIONAR UNA OPCION!";
		JLabel preguntaRegistro12 = new JLabel(textoRegistro12);
		preguntaRegistro12.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 20));
		preguntaRegistro12.setBounds(300, 320, 300, 100);
		preguntaRegistro12.setForeground(Color.decode("#8B0000"));
		ventanaRegistro.getContentPane().add(preguntaRegistro12);
		preguntaRegistro12.setVisible(false);

		String textoRegistro13 = "OPCION NO VALIDA";
		JLabel preguntaRegistro13 = new JLabel(textoRegistro13);
		preguntaRegistro13.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 20));
		preguntaRegistro13.setBounds(300, 320, 300, 100);
		preguntaRegistro13.setForeground(Color.decode("#8B0000"));
		ventanaRegistro.getContentPane().add(preguntaRegistro13);
		preguntaRegistro13.setVisible(false);

		// --------

		JLabel fondoMenu = new JLabel();
		fondoMenu.setIcon(new ImageIcon("Recursos/Ventanas/Formulario/fondo.png"));
		fondoMenu.setBounds(0, 0, 800, 600);
		ventanaRegistro.getContentPane().add(fondoMenu);

		// elementos muro
		// ____________________________________________________________________________________________

		JButton cerrarMuro = new JButton("");
		cerrarMuro.setIcon(iconoCerrar);
		cerrarMuro.setBounds(700, 0, 82, 70);
		ventanaMuro.getContentPane().add(cerrarMuro);

		JButton como_esta_hoy = new JButton();
		como_esta_hoy.setIcon(new ImageIcon("Recursos/Ventanas/Muro/como_esta_hoy.png"));
		como_esta_hoy.setBounds(21, 41, 347, 54);
		ventanaMuro.getContentPane().add(como_esta_hoy);

		String nombre = "nombre";
		JLabel nombreUsuario = new JLabel(nombre);
		nombreUsuario.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 15));
		nombreUsuario.setBounds(666, 48, 300, 100);
		nombreUsuario.setForeground(Color.decode("#000000"));
		ventanaMuro.getContentPane().add(nombreUsuario);

		JButton foto_perfil = new JButton();
		foto_perfil.setIcon(new ImageIcon("Recursos/Ventanas/Muro/foto_perfil.png"));
		foto_perfil.setBounds(539, 18, 113, 90);
		ventanaMuro.getContentPane().add(foto_perfil);

		JLabel barra = new JLabel();
		barra.setIcon(new ImageIcon("Recursos/Ventanas/Muro/barra.png"));
		barra.setBounds(0, 0, 800, 114);
		ventanaMuro.getContentPane().add(barra);

		JButton botonSubirMuro = new JButton("");
		ImageIcon iconoSybir1 = new ImageIcon("Recursos/Ventanas/Muro/subir.png");
		botonSubirMuro.setIcon(iconoSybir1);
		botonSubirMuro.setBounds(650, 150, 100, 100);
		ventanaMuro.getContentPane().add(botonSubirMuro);

		JButton botonBajarMuro = new JButton("");
		ImageIcon iconoBajar1 = new ImageIcon("Recursos/Ventanas/Muro/bajar.png");
		botonBajarMuro.setIcon(iconoBajar1);
		botonBajarMuro.setBounds(650, 430, 100, 100);
		ventanaMuro.getContentPane().add(botonBajarMuro);

		JLabel muro = new JLabel();
		muro.setIcon(new ImageIcon("Recursos/Ventanas/Muro/muro.png"));
		muro.setBounds(0, 0, 800, 3000);
		ventanaMuro.getContentPane().add(muro);
		muro.setVisible(true);

		JLabel fondoMuro = new JLabel();
		fondoMuro.setIcon(new ImageIcon("Recursos/Ventanas/Muro/fondo.png"));
		fondoMuro.setBounds(0, 0, 800, 600);
		ventanaMuro.getContentPane().add(fondoMuro);

		botonSubirMuro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				muro.setBounds(0, muro.getY() + 50, 800, 3000);
			}
		});

		botonBajarMuro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				muro.setBounds(0, muro.getY() - 50, 800, 3000);
			}
		});

		// elementos perfil
		// _____________________________________________________________________________________

		JButton cerrarMiPerfil = new JButton("");
		cerrarMiPerfil.setIcon(iconoCerrar);
		cerrarMiPerfil.setBounds(700, 0, 82, 70);
		ventanaPerfil.getContentPane().add(cerrarMiPerfil);

		JButton sobreMI = new JButton();
		sobreMI.setIcon(new ImageIcon("Recursos/Ventanas/Perfil/miPerfil.png"));
		sobreMI.setBounds(650, 260, 137, 164);
		ventanaPerfil.getContentPane().add(sobreMI);

		String nombre_miPerfil = "nombre";
		JLabel nombreUsuario_miPerfil = new JLabel(nombre_miPerfil);
		nombreUsuario_miPerfil.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 30));
		nombreUsuario_miPerfil.setBounds(24, 90, 300, 100);
		nombreUsuario_miPerfil.setForeground(Color.decode("#000000"));
		ventanaPerfil.getContentPane().add(nombreUsuario_miPerfil);

		JButton foto_perfil_miPerfil = new JButton();
		foto_perfil_miPerfil.setIcon(new ImageIcon("Recursos/Ventanas/Perfil/foto_perfil.png"));
		foto_perfil_miPerfil.setBounds(255, 18, 254, 185);
		ventanaPerfil.getContentPane().add(foto_perfil_miPerfil);

		JButton botonSubirMuro_perfil = new JButton("");
		ImageIcon iconoSybir1_perfil = new ImageIcon("Recursos/Ventanas/Perfil/subir.png");
		botonSubirMuro_perfil.setIcon(iconoSybir1_perfil);
		botonSubirMuro_perfil.setBounds(650, 150, 100, 100);
		ventanaPerfil.getContentPane().add(botonSubirMuro_perfil);

		JLabel barra_perfil = new JLabel();
		barra_perfil.setIcon(new ImageIcon("Recursos/Ventanas/Perfil/barra.png"));
		barra_perfil.setBounds(0, -1, 800, 212);
		ventanaPerfil.getContentPane().add(barra_perfil);

		JButton botonBajarMuro_perfil = new JButton("");
		ImageIcon iconoBajar1_perfil = new ImageIcon("Recursos/Ventanas/Perfil/bajar.png");
		botonBajarMuro_perfil.setIcon(iconoBajar1_perfil);
		botonBajarMuro_perfil.setBounds(650, 430, 100, 100);
		ventanaPerfil.getContentPane().add(botonBajarMuro_perfil);

		JLabel muro_perfil = new JLabel();
		muro_perfil.setIcon(new ImageIcon("Recursos/Ventanas/Perfil/muro.png"));
		muro_perfil.setBounds(0, 0, 800, 3000);
		ventanaPerfil.getContentPane().add(muro_perfil);
		muro_perfil.setVisible(true);

		JLabel fondoMuro_perfil = new JLabel();
		fondoMuro_perfil.setIcon(new ImageIcon("Recursos/Ventanas/Perfil/fondo.png"));
		fondoMuro_perfil.setBounds(0, 0, 800, 600);
		ventanaPerfil.getContentPane().add(fondoMuro_perfil);

		botonSubirMuro_perfil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				muro_perfil.setBounds(0, muro_perfil.getY() + 50, 800, 3000);
			}
		});

		botonBajarMuro_perfil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				muro_perfil.setBounds(0, muro_perfil.getY() - 50, 800, 3000);
			}
		});

		// elementos sobre mi
		// ____________________________________________________________________________________

		JButton cerrarSobreMi = new JButton("");
		cerrarSobreMi.setIcon(iconoCerrar);
		cerrarSobreMi.setBounds(700, 0, 82, 70);
		ventanaSobreMi.getContentPane().add(cerrarSobreMi);

		String nombre_miPerfil1 = "";
		JLabel nombreUsuario_miPerfil1 = new JLabel(nombre_miPerfil1);
		nombreUsuario_miPerfil1.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 15));
		nombreUsuario_miPerfil1.setBounds(122, 120, 300, 100);
		nombreUsuario_miPerfil1.setForeground(Color.decode("#000000"));
		ventanaSobreMi.getContentPane().add(nombreUsuario_miPerfil1);

		String direccion_miPerfilt = "";
		JLabel direccion_miPerfil = new JLabel(direccion_miPerfilt);
		direccion_miPerfil.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 15));
		direccion_miPerfil.setBounds(122, 320, 300, 100);
		direccion_miPerfil.setForeground(Color.decode("#000000"));
		ventanaSobreMi.getContentPane().add(direccion_miPerfil);

		String nenenena_miPerfilt = "";
		JLabel nenenena_miPerfil = new JLabel(nenenena_miPerfilt);
		nenenena_miPerfil.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 15));
		nenenena_miPerfil.setBounds(122, 220, 300, 100);
		nenenena_miPerfil.setForeground(Color.decode("#000000"));
		ventanaSobreMi.getContentPane().add(nenenena_miPerfil);

		String papas_miPerfilt = "";
		JLabel papas_miPerfil = new JLabel(papas_miPerfilt);
		papas_miPerfil.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 15));
		papas_miPerfil.setBounds(122, 420, 300, 100);
		papas_miPerfil.setForeground(Color.decode("#000000"));
		ventanaSobreMi.getContentPane().add(papas_miPerfil);

		String hermanos_miPerfilt = "";
		JLabel hermanos_miPerfil = new JLabel(hermanos_miPerfilt);
		hermanos_miPerfil.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 15));
		hermanos_miPerfil.setBounds(122, 520, 300, 100);
		hermanos_miPerfil.setForeground(Color.decode("#000000"));
		ventanaSobreMi.getContentPane().add(hermanos_miPerfil);

		String mascotas_miPerfilt = "";
		JLabel mascotas_miPerfil = new JLabel(mascotas_miPerfilt);
		mascotas_miPerfil.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 15));
		mascotas_miPerfil.setBounds(486, 120, 300, 100);
		mascotas_miPerfil.setForeground(Color.decode("#000000"));
		ventanaSobreMi.getContentPane().add(mascotas_miPerfil);

		String telefono_miPerfilt = "";
		JLabel telefono_miPerfil = new JLabel(telefono_miPerfilt);
		telefono_miPerfil.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 15));
		telefono_miPerfil.setBounds(486, 220, 300, 100);
		telefono_miPerfil.setForeground(Color.decode("#000000"));
		ventanaSobreMi.getContentPane().add(telefono_miPerfil);

		String colegio_miPerfilt = "";
		JLabel colegio_miPerfil = new JLabel(colegio_miPerfilt);
		colegio_miPerfil.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 15));
		colegio_miPerfil.setBounds(486, 320, 300, 100);
		colegio_miPerfil.setForeground(Color.decode("#000000"));
		ventanaSobreMi.getContentPane().add(colegio_miPerfil);

		String maestro_miPerfilt = "";
		JLabel maestro_miPerfil = new JLabel(maestro_miPerfilt);
		maestro_miPerfil.setFont(new Font("Tahoma", Font.BOLD | Font.BOLD, 15));
		maestro_miPerfil.setBounds(486, 420, 300, 100);
		maestro_miPerfil.setForeground(Color.decode("#000000"));
		ventanaSobreMi.getContentPane().add(maestro_miPerfil);

		JLabel fondoObjetos = new JLabel();
		fondoObjetos.setIcon(new ImageIcon("Recursos/Ventanas/SobreMi/fondo.png"));
		fondoObjetos.setBounds(0, 0, 800, 600);
		ventanaSobreMi.getContentPane().add(fondoObjetos);

		// _________________________________________________________________________________________

		JButton soleado = new JButton("");
		ImageIcon iconosoleado = new ImageIcon("Recursos/Ventanas/Muro/Dia/soleado.png");
		soleado.setIcon(iconosoleado);
		soleado.setBounds(14, 14, 203, 249);
		ventanaDia.getContentPane().add(soleado);

		JButton sol_y_nubes = new JButton("");
		ImageIcon iconosol_y_nubes = new ImageIcon("Recursos/Ventanas/Muro/Dia/sol_y_nubes.png");
		sol_y_nubes.setIcon(iconosol_y_nubes);
		sol_y_nubes.setBounds(238, 14, 203, 249);
		ventanaDia.getContentPane().add(sol_y_nubes);

		JButton muchas_nubes = new JButton("");
		ImageIcon iconomuchas_nubes = new ImageIcon("Recursos/Ventanas/Muro/Dia/muchas_nubes.png");
		muchas_nubes.setIcon(iconomuchas_nubes);
		muchas_nubes.setBounds(460, 14, 203, 249);
		ventanaDia.getContentPane().add(muchas_nubes);

		JButton nublado = new JButton("");
		ImageIcon icononublado = new ImageIcon("Recursos/Ventanas/Muro/Dia/nublado.png");
		nublado.setIcon(icononublado);
		nublado.setBounds(684, 14, 203, 249);
		ventanaDia.getContentPane().add(nublado);

		JButton lluvia = new JButton("");
		ImageIcon iconolluvia = new ImageIcon("Recursos/Ventanas/Muro/Dia/lluvia.png");
		lluvia.setIcon(iconolluvia);
		lluvia.setBounds(14, 284, 203, 249);
		ventanaDia.getContentPane().add(lluvia);

		JButton tormenta = new JButton("");
		ImageIcon iconotormenta = new ImageIcon("Recursos/Ventanas/Muro/Dia/tormenta.png");
		tormenta.setIcon(iconotormenta);
		tormenta.setBounds(234, 284, 203, 249);
		ventanaDia.getContentPane().add(tormenta);

		JButton viento = new JButton("");
		ImageIcon iconoviento = new ImageIcon("Recursos/Ventanas/Muro/Dia/viento.png");
		viento.setIcon(iconoviento);
		viento.setBounds(460, 284, 203, 249);
		ventanaDia.getContentPane().add(viento);

		JButton nieve = new JButton("");
		ImageIcon icononieve = new ImageIcon("Recursos/Ventanas/Muro/Dia/nieve.png");
		nieve.setIcon(icononieve);
		nieve.setBounds(684, 284, 203, 249);
		ventanaDia.getContentPane().add(nieve);

		JLabel fondoDia = new JLabel();
		fondoDia.setIcon(new ImageIcon("Recursos/Ventanas/Muro/Dia/fondo.png"));
		fondoDia.setBounds(0, 0, 800, 600);
		ventanaSobreMi.getContentPane().add(fondoDia);

		// __________________________________________________________________________________________

		botonEntrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaLogin.setVisible(false);
				ventanaSeleccionUsuario.setVisible(true);

			}
		});

		botonUsuarioPrincipal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (existeUsuarioPrincipal == false) {
					ventanaLogin.setVisible(false);
					ventanaMuro.setVisible(true);
					ventanaSeleccionUsuario.setVisible(false);
					nombreUsuario.setText(nombre1FER);
					nombreUsuario_miPerfil.setText(nombre1FER);
					nombreUsuario_miPerfil1.setText(nombre1FER + " " + apellidoFER);
					direccion_miPerfil.setText(direccionFER + " ," + donde_viveFER);
					nenenena_miPerfil.setText(sexoFER);
					papas_miPerfil.setText(papasFER);
					hermanos_miPerfil.setText(hermanosFER);
					mascotas_miPerfil.setText(mascotasFER);
					telefono_miPerfil.setText(telefonoFER);
					colegio_miPerfil.setText(colegioFER);
					maestro_miPerfil.setText(maestroFER);
				}
			}
		});

		botonNuevoUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaSeleccionUsuario.setVisible(false);
				ventanaRegistro.setVisible(true);
				preguntaRegistro0.setVisible(true);
				pictonene.setVisible(true);
				pictonena.setVisible(true);
				neneOK.setVisible(true);
				nenaOK.setVisible(true);
				contadorRegistro = 1;

			}
		});

		seguirRegistro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (contadorRegistro == 11) {

					pictofoto.setVisible(false);
					botonsubirRegistro.setVisible(false);
					preguntaRegistro11.setVisible(false);
					ventanaMuro.setVisible(true);
					ventanaRegistro.setVisible(false);
					contadorRegistro = 0;
					nombreUsuario.setText(nombre1);
					nombreUsuario_miPerfil.setText(nombre1);
					nombreUsuario_miPerfil1.setText(nombre1 + " " + apellido);
					direccion_miPerfil.setText(direccion + " ," + donde_vive);
					nenenena_miPerfil.setText(sexo);
					papas_miPerfil.setText(papas);
					hermanos_miPerfil.setText(hermanos);
					mascotas_miPerfil.setText(mascotas);
					telefono_miPerfil.setText(telefono);
					colegio_miPerfil.setText(colegio);
					maestro_miPerfil.setText(maestro);

				}
				if (contadorRegistro == 10) {
					if (!texto_ingreso_registro_10.getText().equals("")) {
						texto_ingreso_registro_10.setVisible(false);
						pictomaestra.setVisible(false);
						preguntaRegistro10.setVisible(false);
						pictofoto.setVisible(true);
						botonsubirRegistro.setVisible(true);
						preguntaRegistro11.setVisible(true);
						contadorRegistro = 11;
						maestro = texto_ingreso_registro_10.getText();
					}
				}
				if (contadorRegistro == 9) {
					if (!texto_ingreso_registro_9.getText().equals("")) {
						preguntaRegistro9.setVisible(false);
						pictocolegio.setVisible(false);
						texto_ingreso_registro_9.setVisible(false);
						texto_ingreso_registro_10.setVisible(true);
						pictomaestra.setVisible(true);
						preguntaRegistro10.setVisible(true);
						contadorRegistro = 10;
						colegio = texto_ingreso_registro_9.getText();
					}
				}
				if (contadorRegistro == 8) {
					if (!texto_ingreso_registro_8.getText().equals("")) {
						preguntaRegistro8.setVisible(false);
						pictoperro.setVisible(false);
						pictogato.setVisible(false);
						texto_ingreso_registro_8.setVisible(false);
						preguntaRegistro9.setVisible(true);
						pictocolegio.setVisible(true);
						texto_ingreso_registro_9.setVisible(true);
						contadorRegistro = 9;
						mascotas = texto_ingreso_registro_8.getText();
					}

				}
				if (contadorRegistro == 7) {
					if (!texto_ingreso_registro_7.getText().equals("")) {
						texto_ingreso_registro_7.setVisible(false);
						pictohermano.setVisible(false);
						preguntaRegistro7.setVisible(false);
						preguntaRegistro8.setVisible(true);
						pictoperro.setVisible(true);
						pictogato.setVisible(true);
						texto_ingreso_registro_8.setVisible(true);
						contadorRegistro = 8;
						hermanos = texto_ingreso_registro_7.getText();
					}

				}
				if (contadorRegistro == 6) {
					if (!texto_ingreso_registro_6.getText().equals("")) {
						texto_ingreso_registro_6.setVisible(false);
						pictomama.setVisible(false);
						pictopapa.setVisible(false);
						preguntaRegistro6.setVisible(false);
						texto_ingreso_registro_7.setVisible(true);
						pictohermano.setVisible(true);
						preguntaRegistro7.setVisible(true);
						contadorRegistro = 7;
						papas = texto_ingreso_registro_6.getText();
					}
				}
				if (contadorRegistro == 5) {
					if (!texto_ingreso_registro_5.getText().equals("")) {
						texto_ingreso_registro_5.setVisible(false);
						pictotelefono.setVisible(false);
						preguntaRegistro5.setVisible(false);
						texto_ingreso_registro_6.setVisible(true);
						pictomama.setVisible(true);
						pictopapa.setVisible(true);
						preguntaRegistro6.setVisible(true);
						contadorRegistro = 6;
						telefono = texto_ingreso_registro_5.getText();
					}

				}
				if (contadorRegistro == 4) {
					if (!texto_ingreso_registro_4.getText().equals("")) {
						texto_ingreso_registro_4.setVisible(false);
						pictodireccion.setVisible(false);
						preguntaRegistro4.setVisible(false);
						texto_ingreso_registro_5.setVisible(true);
						pictotelefono.setVisible(true);
						preguntaRegistro5.setVisible(true);
						contadorRegistro = 5;
						direccion = texto_ingreso_registro_4.getText();
					}
				}
				if (contadorRegistro == 3) {
					if (!texto_ingreso_registro_3.getText().equals("")) {
						texto_ingreso_registro_3.setVisible(false);
						pictodonde_vivis.setVisible(false);
						preguntaRegistro3.setVisible(false);
						texto_ingreso_registro_4.setVisible(true);
						pictodireccion.setVisible(true);
						preguntaRegistro4.setVisible(true);
						contadorRegistro = 4;
						donde_vive = texto_ingreso_registro_3.getText();
					}

				}
				if (contadorRegistro == 2) {
					if (!texto_ingreso_registro_1.getText().equals("")
							&& !texto_ingreso_registro_2.getText().equals("")) {
						pictotu_nombre.setVisible(false);
						texto_ingreso_registro_2.setVisible(false);
						preguntaRegistro1.setVisible(false);
						preguntaRegistro2.setVisible(false);
						texto_ingreso_registro_1.setVisible(false);
						texto_ingreso_registro_3.setVisible(true);
						pictodonde_vivis.setVisible(true);
						preguntaRegistro3.setVisible(true);
						contadorRegistro = 3;
						nombre1 = texto_ingreso_registro_1.getText();
						apellido = texto_ingreso_registro_2.getText();
					}

				}
				if (contadorRegistro == 1) {

					if (neneOK.isSelected() && !nenaOK.isSelected()) {
						sexo = "nene";
						preguntaRegistro0.setVisible(false);
						pictonene.setVisible(false);
						pictonena.setVisible(false);
						pictotu_nombre.setVisible(true);
						texto_ingreso_registro_2.setVisible(true);
						preguntaRegistro1.setVisible(true);
						preguntaRegistro2.setVisible(true);
						texto_ingreso_registro_1.setVisible(true);
						contadorRegistro = 2;
						neneOK.setVisible(false);
						nenaOK.setVisible(false);
					}
					if (nenaOK.isSelected() && !neneOK.isSelected()) {
						sexo = "nena";
						preguntaRegistro0.setVisible(false);
						pictonene.setVisible(false);
						pictonena.setVisible(false);
						pictotu_nombre.setVisible(true);
						texto_ingreso_registro_2.setVisible(true);
						preguntaRegistro1.setVisible(true);
						preguntaRegistro2.setVisible(true);
						texto_ingreso_registro_1.setVisible(true);
						contadorRegistro = 2;
						neneOK.setVisible(false);
						nenaOK.setVisible(false);
					}

				}

			}
		});

		como_esta_hoy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaDia.setVisible(true);

			}
		});

		foto_perfil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaMuro.setVisible(false);
				ventanaPerfil.setVisible(true);

			}
		});
		soleado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaDia.setVisible(false);

			}
		});
		sol_y_nubes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaDia.setVisible(false);

			}
		});
		muchas_nubes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaDia.setVisible(false);

			}
		});
		nublado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaDia.setVisible(false);

			}
		});
		lluvia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaDia.setVisible(false);

			}
		});
		tormenta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaDia.setVisible(false);

			}
		});
		viento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaDia.setVisible(false);

			}
		});
		nieve.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaDia.setVisible(false);

			}
		});

		sobreMI.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaSobreMi.setVisible(true);

			}
		});

		foto_perfil_miPerfil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// aca deberia llamar a cambiar foto
			}
		});

		cerrarSobreMi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaSobreMi.setVisible(false);
			}
		});

		cerrarMiPerfil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaPerfil.setVisible(false);
				ventanaMuro.setVisible(true);
			}
		});

		cerrarMuro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaMuro.setVisible(false);
				ventanaLogin.setVisible(true);

			}
		});

		cerrarRegistro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaRegistro.setVisible(false);
				ventanaLogin.setVisible(true);

			}
		});

		cerrarSeleccionUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventanaSeleccionUsuario.setVisible(false);
				ventanaLogin.setVisible(true);

			}
		});

		cerrarLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);

			}
		});

	}

}
